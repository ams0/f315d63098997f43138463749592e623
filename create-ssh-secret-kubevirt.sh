cat << END > startup-script
#cloud-config
hostname: vm1
ssh_authorized_keys:
  - $(cat ~/.ssh/id_rsa.pub)
END

kubectl create secret generic vm1-cloudconfig --from-file=userdata=startup-script
